---
title: "Locuswise hits by condition and condition or sex specific variants; varadb score dists 092621.txt"
author: "Bernie Mulvey"
date: "9/26/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11)))
```



## locuswise facet plotting for # hit snps
#### ugh, 31 loci.. closest two tags that are mdd loci and both nonzero are rs11082011 and rs1187264 (144kb apart on chr18:37.55 and 37.70 mbp respectively)
###### also ugh: data.table[,lapply(.SD,sum),by="",.SDcols] is not returning accurate values. not sure what's going on here.
```{r}
locuswise.sc <- as.data.table(unique(mdd2anno[,.(rsid_only,ldblock_id,tag_snp)]))
setnames(locuswise.sc,1,"rsid_only")
i<-1
for (i in c(1:8)){
  locuswise.sc <- merge.data.table(locuswise.sc,multithresh[[i]][,c(1,14)],by="rsid_only",all.x=T)
  setnames(locuswise.sc,(i+3),gsub(names(locuswise.sc)[i+3],pattern="^(.*)_Pempal.*$",replacement="\\1"))
  getname <- names(locuswise.sc)[i+3]
  locuswise.sc[,c(getname):=as.numeric(get(getname))]
  rm(getname)
}
rm(i)
### combine the two CELF4 tags into a single locus so there's 30 loci rather than 31. see notes below.
locuswise.sc[tag_snp %in% c("rs1187264","rs11082011"),tag_snp:="CELF4‡"]
locuswise.sc[tag_snp %in% c("rs11663393","rs12958048","rs12966052","rs2060886","rs68021739"),tag_snp:="TCF4‡‡"]
pct_reftab <- locuswise.sc[,lapply(.SD,FUN=function(x){sum(!is.na(x))}),.SDcols=c(4:11),by="tag_snp"]
setnames(pct_reftab,c(2:9),paste0(names(pct_reftab)[c(2:9)],"_analyzed"))

locuswise.sc[,keep:=rowSums(locuswise.sc[,c(4:11)],na.rm=T)]
locuswise.sc <- locuswise.sc[keep>0]
locuswise.sc[,keep:=NULL]
setnafill(x=locuswise.sc,cols = c(4:11),fill = 0)

locuswise.sc.res <- locuswise.sc[,lapply(.SD,sum),by="tag_snp",.SDcols=c(4:11)]
locuswise.sc.res <- merge.data.table(locuswise.sc.res,pct_reftab,by="tag_snp")
rm(pct_reftab)

i<-2
for (i in c(2:9)){
  cola <- names(locuswise.sc.res)[i]
  colb <- names(locuswise.sc.res)[i+8]
  locuswise.sc.res[,newcol:=get(cola)/get(colb)]
  setnames(locuswise.sc.res,"newcol",paste0(cola,"_pcthit"))
  rm(cola,colb)
}
locuswise.sc.res <- locuswise.sc.res[,c(1,18:25)]

locuswise.int <- as.data.table(unique(mdd2anno[,.(rsid_only,ldblock_id,tag_snp)]))
locuswise.int[tag_snp %in% c("rs1187264","rs11082011"),tag_snp:="CELF4‡"]
locuswise.int[tag_snp %in% c("rs11663393","rs12958048","rs12966052","rs2060886","rs68021739"),tag_snp:="TCF4‡‡"]
i<-9
for (i in c(9:12)){
  locuswise.int <- merge.data.table(locuswise.int,multithresh[[i]][,c(1,22)],by="rsid_only",all.x=T)
  setnames(locuswise.int,(i-5),gsub(names(locuswise.int)[i-5],pattern="^(.*)_Pempint.*$",replacement="\\1"))
  getname <- names(locuswise.int)[i-5]
  locuswise.int[,c(getname):=as.numeric(get(getname))]
  rm(getname)
}
rm(i)

pct_reftab <- locuswise.int[,lapply(.SD,FUN=function(x){sum(!is.na(x))}),.SDcols=c(4:7),by="tag_snp"]
setnames(pct_reftab,c(2:5),paste0(names(pct_reftab)[c(2:5)],"_analyzed"))

locuswise.int[,keep:=rowSums(locuswise.int[,c(4:7)],na.rm=T)]
locuswise.int <- locuswise.int[keep>0]
locuswise.int[,keep:=NULL]
setnafill(x=locuswise.int,cols = c(4:7),fill = 0)

locuswise.int.res <- locuswise.int[,lapply(.SD,sum),by="tag_snp",.SDcols=c(4:7)]
locuswise.int.res <- merge.data.table(locuswise.int.res,pct_reftab,by="tag_snp")
rm(pct_reftab)

i<-2
for (i in c(2:5)){
  cola <- names(locuswise.int.res)[i]
  colb <- names(locuswise.int.res)[i+4]
  locuswise.int.res[,newcol:=get(cola)/get(colb)]
  setnames(locuswise.int.res,"newcol",paste0(cola,"_pcthit"))
  rm(cola,colb)
}
rm(i)
locuswise.int.res <- locuswise.int.res[,c(1,10:13)]

locuswise.res <- merge.data.table(locuswise.sc.res,locuswise.int.res,by="tag_snp",all=T)
rm(locuswise.sc.res,locuswise.int.res)

locuswise.res.note <- merge.data.table(locuswise.res,unique(mdd2anno[,.(tag_snp,ldblock_id)]),by="tag_snp",all.x=T)

locuswise.res.note[is.na(ldblock_id),ldblock_id:=paste0(unique(mdd2anno[tag_snp=="rs11082011",ldblock_id])," and ", unique(mdd2anno[tag_snp=="rs1187264",ldblock_id]))]

specmat <- merge.data.table(locuswise.int,locuswise.sc,by="rsid_only",all=T)
specmat[,ldblock_id.y:=NULL]
specmat[,tag_snp.y:=NULL]
rm(locuswise.int,locuswise.sc)
specmat[,fhit:=rowSums(specmat[,.(fz,ft,fpre,fip)],na.rm=T)]
specmat[,mhit:=rowSums(specmat[,.(mz,mt,mpre,mip)],na.rm=T)]
specmat[,zhit:=rowSums(specmat[,.(mz,fz,p0int)],na.rm=T)]
specmat[,thit:=rowSums(specmat[,.(mt,ft)],na.rm=T)]
specmat[,hiphit:=rowSums(specmat[,.(fpre,mpre,hipint)],na.rm=T)]
specmat[,gluhit:=rowSums(specmat[,.(mip,fip,gluint)],na.rm=T)]
specmat[,int:=rowSums(specmat[,.(p0int,hipint,gluint)],na.rm=T)]

# fem spec, male spec
nrow(specmat[fhit>0&mhit==0])
nrow(specmat[mhit>0&fhit==0])
# fem spec, male spec (excluding p10)
nrow(specmat[(fhit-ft)>0&(mhit-mt)==0])
nrow(specmat[(mhit-mt)>0&(fhit-ft)==0])

#P0, P10, Hip, Glu spec (including interactions)
nrow(specmat[zhit>0&thit==0&hiphit==0&gluhit==0])
nrow(specmat[zhit==0&thit>0&hiphit==0&gluhit==0])
nrow(specmat[zhit==0&thit==0&hiphit>0&gluhit==0])
nrow(specmat[zhit==0&thit==0&hiphit==0&gluhit>0])
# write.table(specmat,"Analysis PLOTting and plots/FDR20 binary hit status and times hit by sex and condition 100521.txt",sep='\t',quote=F,row.names=F,col.names=T)
rm(specmat)

setnames(locuswise.res,c(2:13),gsub(names(locuswise.res)[c(2:13)],pattern="(.*)_pcthit",replacement="\\1"))

## subset to mdd loci: drop Ward19_ARHGAP15, MeierTrontti18_PDE4B, Grove19_PTPB, Grove19_CADPS, Demontis19_DUSP6, Demontis19_CADPS2,Debette14Hill18Lee18_PTGER3-Blk1,Debette14Hill18Lee18_PTGER3-Blk2,Debette14Hill18Lee18_PTGER3-Blk3

droptags <- unique(mdd2anno[ldblock_id %in% c("Ward19_ARHGAP15", "MeierTrontti18_PDE4B", "Grove19_PTPB", "Grove19_CADPS", "Demontis19_DUSP6", "Demontis19_CADPS2","Debette14Hill18Lee18_PTGER3-Blk2","Debette14Hill18Lee18_PTGER3-Blk1","Debette14Hill18Lee18_PTGER3-Blk3"),tag_snp])
locuswise.res <- locuswise.res[!(tag_snp %in% droptags)&!is.na(tag_snp)]
rm(droptags)

locuswise.res.plt <- melt(locuswise.res,id.vars = "tag_snp")
locuswise.res.plt[variable=="fz",variable:="P0 F"]
locuswise.res.plt[variable=="mz",variable:="P0 M"]
locuswise.res.plt[variable=="p0int",variable:="P0 SxG"]
locuswise.res.plt[variable=="ft",variable:="P10 F"]
locuswise.res.plt[variable=="mt",variable:="P10 M"]
locuswise.res.plt[variable=="fpre",variable:="F Hippocampus"]
locuswise.res.plt[variable=="mpre",variable:="M Hippocampus"]
locuswise.res.plt[variable=="hipint",variable:="SxG Hippocampus"]
locuswise.res.plt[variable=="fip",variable:="F Vglut1"]
locuswise.res.plt[variable=="mip",variable:="M Vglut1"]
locuswise.res.plt[variable=="gluint",variable:="SxG Vglut1"]



locuswise.res.plt[,variable:=factor(variable,levels = c("P0 F","P0 M","P0 SxG","P10 F","P10 M","M Hippocampus","F Hippocampus","SxG Hippocampus","F Vglut1","M Vglut1","SxG Vglut1"))]

setnames(locuswise.res.plt,"variable","Condition")
### asterisk up certain tag sets to note male/female discovery origin and non-MDD associations
# locuswise.res.plt[tag_snp=="rs1883640",tag_snp:="rs1883640#"]
# locuswise.res.plt[tag_snp=="rs481940",tag_snp:="rs481940#"]
locuswise.res.plt[tag_snp=="rs12415800",tag_snp:="rs12415800*"]
locuswise.res.plt[tag_snp=="rs35936514",tag_snp:="rs35936514*"]
locuswise.res.plt[tag_snp=="rs7950328",tag_snp:="rs7950328**"]
locuswise.res.plt[tag_snp=="rs9323497",tag_snp:="rs9323497**"]

cairo_pdf("Percent hits per condition by locus facet plot-astCVRG_2astPower17_crsCELFmerge_2crsTCF4_5tags 101321.pdf",height=4,width=5)
ggplot(locuswise.res.plt[value>0],aes(y=value,x=Condition,fill=Condition))+
  geom_col(position="dodge")+
  facet_wrap(.~tag_snp,nrow = 5)+
  xlab("Condition")+
  ylab("% rSNPs (FDR <0.2)")+
  theme(strip.text.x =  element_text(size=9),axis.text.x = element_blank(),axis.title.x = element_blank(),axis.text.y=element_text(size=9),axis.title.y=element_text(size=10),legend.title = element_text(size=10),legend.text=element_text(size=9))
dev.off()

rm(locuswise.res.plt,locuswise.res,locuswise.res.note,pct_reftab)
```

VARAdb scores for the 280 FDR 20 hits, vs. VARAdb-wide noncoding SNP scores
```{r}
varadbdist <- fread("Analysis PLOTting and plots/VARAdb fxnl annot scores/varadb score dist.txt",header=F)
setnames(varadbdist,c("score","nvars"))
varadbres <- fread("Analysis PLOTting and plots/VARAdb fxnl annot scores/VARAdb_FDR20hits.csv")
varadbres <- as.data.table(table(varadbres$Score))
setnames(varadbres,c("score","nhits"))
varadbres[,score:=as.integer(score)]

varadbrates <- merge.data.table(varadbdist,varadbres,by="score")
varadbrates[,`Genomewide`:=nvars/sum(nvars)]
varadbrates[,`MPRA rSNPs`:=nhits/sum(nhits)]

varadbrates <- melt(varadbrates[,c(1,4,5)],id.vars="score")
setnames(varadbrates,"variable","VARAdb Scores")

pdf("Analysis PLOTting and plots/VARAdb scores-genomewide distribution vs distribution for FDR 20 rSNPs 092621.pdf",height=4,width=5)
ggplot(varadbrates,aes(x=score,y=value,fill=`VARAdb Scores`))+
  geom_col(position="dodge")+
  ylab("Proportion of Noncoding Variants")+
  xlab("VARAdb Score")+
  theme(axis.title = element_text(size=10),axis.text=element_text(size=9),legend.title = element_text(size=10),legend.text=element_text(size=9))
dev.off()

rm(varadb,varadbdist,varadbplt,varadbres,varadbrates)
```