TF	cond	nHitsnp.TFmatches	FDR	depleted	comparison	db	signifconds	name
FOXA1	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::FOXA1
FOXI2,FOXI3,FOXJ3	pZ	5	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::FOXI2,FOXI3,FOXJ3
KLF15	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p10_IVA_p10_IVN	p0_IVA_p10_IVA_p10_IVN:::KLF15
KLF17	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::KLF17
KLF3	pZ	5	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::KLF3
KLF6	pZ	5	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA	p0_IVA:::KLF6
LIN54	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::LIN54
PATZ1	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::PATZ1
PAX5	pZ	5	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::PAX5
SP2	pZ	7	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::SP2
SP6	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA	p0_IVA:::SP6
TFAP2D	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN:::TFAP2D
ZFY	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZFY
ZNF341	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZNF341
cccctcccccacccc	pZ	4	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::cccctcccccacccc
scsccggccccgccyccysscs	pZ	5	2.40636042402827e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::scsccggccccgccyccysscs
EGR4	pZ	5	0.000694012738853234	NA	IVN	cisBP19	p0_IVN_Hip_IVA_Glu_IVA	p0_IVN_Hip_IVA_Glu_IVA:::EGR4
FOXA1	pZ	4	0.00135354037267122	NA	IVN	cisBP19	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::FOXA1
FOXI2,FOXI3,FOXJ3	pZ	5	0.0207395454545456	NA	IVN	cisBP19	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::FOXI2,FOXI3,FOXJ3
KLF17	pZ	4	0.000862025316455601	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::KLF17
KLF3	pZ	5	0.0414862068965517	NA	IVN	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::KLF3
LIN54	pZ	4	0.0062734545454544	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::LIN54
PATZ1	pZ	4	0.042883542857143	NA	IVN	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::PATZ1
PAX5	pZ	5	0.0482343646408842	NA	IVN	cisBP19	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::PAX5
SP2	pZ	7	0.00417791411042899	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::SP2
TFAP2D	pZ	4	0.0419917478510028	NA	IVN	cisBP19	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN:::TFAP2D
WT1	pZ	4	0.0414862068965517	NA	IVN	cisBP19	p0_IVN_p10_IVA_p10_IVN	p0_IVN_p10_IVA_p10_IVN:::WT1
ZFY	pZ	4	0.00614560975609725	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZFY
ZNF341	pZ	4	0.0205082758620686	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZNF341
cccctcccccacccc	pZ	4	0.000174615384615559	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::cccctcccccacccc
scsccggccccgccyccysscs	pZ	5	0.0207024	NA	IVN	cisBP19	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::scsccggccccgccyccysscs
ZNF148	pZ	3	2.10769230769231e-05	NA	IVA	Jasp20	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZNF148
ZNF384	pZ	3	2.10769230769231e-05	NA	IVA	Jasp20	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::ZNF384
ZNF148	pZ	3	0.00111312500000016	NA	IVN	Jasp20	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::ZNF148
ZNF384	pZ	3	0.00111312500000016	NA	IVN	Jasp20	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::ZNF384
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	pZ	5	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pZ	5	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CGkCTaCGm	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::CGkCTaCGm
EGR1_GM12878_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pZ	4	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::EGR1_GM12878_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
FOXP3_transfac_M00992	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::FOXP3_transfac_M00992
HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic	pZ	4	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA:::HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic
MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic
RAD21_HepG2_encode-Snyder_seq_hsa_IgG-rab_r1:AlignACE_3_Intergenic	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_Hip_IVA	p0_IVA_Hip_IVA:::RAD21_HepG2_encode-Snyder_seq_hsa_IgG-rab_r1:AlignACE_3_Intergenic
REST_HTB-11_encode-Myers_seq_hsa_PCR2x_r1:AlignACE_2_Intergenic	pZ	4	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA	p0_IVA:::REST_HTB-11_encode-Myers_seq_hsa_PCR2x_r1:AlignACE_2_Intergenic
RREB-1_transfac_M00257	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::RREB-1_transfac_M00257
SMAD_transfac_M00792	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::SMAD_transfac_M00792
SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic
srSstGvsss	pZ	4	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::srSstGvsss
wCGCGGGA	pZ	3	2.26789838337182e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::wCGCGGGA
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	pZ	5	4.32599118942731e-05	TRUE	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pZ	5	0.00886700421940941	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CGkCTaCGm	pZ	3	0.00370403508771918	NA	IVN	RSAT17	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::CGkCTaCGm
EGR1_GM12878_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pZ	4	0.0133946443514646	NA	IVN	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::EGR1_GM12878_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic	pZ	3	4.32599118942731e-05	TRUE	IVN	RSAT17	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic
EP300_T-47D_encode-Myers_seq_hsa_DMSO-0.02pct-v041610.2_r1:MDscan_3_Intergenic	pZ	3	4.32599118942731e-05	TRUE	IVN	RSAT17	p0_IVN	p0_IVN:::EP300_T-47D_encode-Myers_seq_hsa_DMSO-0.02pct-v041610.2_r1:MDscan_3_Intergenic
FOXP3_transfac_M00992	pZ	3	4.32599118942731e-05	NA	IVN	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::FOXP3_transfac_M00992
MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic	pZ	3	4.32599118942731e-05	TRUE	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic
RREB-1_transfac_M00257	pZ	3	4.32599118942731e-05	NA	IVN	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::RREB-1_transfac_M00257
SMAD_transfac_M00792	pZ	3	4.32599118942731e-05	NA	IVN	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::SMAD_transfac_M00792
SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic	pZ	3	4.32599118942731e-05	TRUE	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic
srSstGvsss	pZ	4	0.0139116666666665	NA	IVN	RSAT17	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::srSstGvsss
wCGCGGGA	pZ	3	4.32599118942731e-05	NA	IVN	RSAT17	p0_IVA_p0_IVN	p0_IVA_p0_IVN:::wCGCGGGA
AHR	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::AHR
ATF3	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA	p10_IVA:::ATF3
CEBPG	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CEBPG
CREBL2	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CREBL2
E2F1	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F1
E2F2	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F2
E2F4	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F4
E2F7	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F7
FBXL19,KDM2A	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::FBXL19,KDM2A
HHEX	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::HHEX
KLF12	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::KLF12
KLF15	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p0_IVA_p10_IVA_p10_IVN	p0_IVA_p10_IVA_p10_IVN:::KLF15
KLF3	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::KLF3
KLF4	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::KLF4
NHLH1	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN:::NHLH1
NR3C1	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::NR3C1
PATZ1	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_p10_IVA	p0_IVA_p0_IVN_p10_IVA:::PATZ1
PAX4	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_Hip_IVA	p10_IVA_Hip_IVA:::PAX4
PAX6	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::PAX6
SP3,SP6	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA	p10_IVA:::SP3,SP6
SP4	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::SP4
SREBF1	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA	p10_IVA:::SREBF1
TFAP4	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::TFAP4
TFDP1,TFDP2,TFDP3	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::TFDP1,TFDP2,TFDP3
WT1	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p0_IVN_p10_IVA_p10_IVN	p0_IVN_p10_IVA_p10_IVN:::WT1
ZBTB12	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_Hip_IVA_Hip_IVN	p10_IVA_Hip_IVA_Hip_IVN:::ZBTB12
ZBTB14	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVA_p10_IVN_Glu_IVA:::ZBTB14
ZIC3	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZIC3
ZNF180	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA	p10_IVA:::ZNF180
ZNF263	pT	5	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF263
ZNF30	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::ZNF30
ZNF460	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::ZNF460
ZNF571	pT	4	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF571
ZNF8	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF8
cccrgccccrccchycycc	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cccrgccccrccchycycc
cccrgccmcrccch	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cccrgccmcrccch
cgtgygcam	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cgtgygcam
grgggcgggrmr	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::grgggcgggrmr
rgggggcgggrmmw	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::rgggggcgggrmmw
rgsmycagctgcgyy	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::rgsmycagctgcgyy
shgrcgcsrvcrsms	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::shgrcgcsrvcrsms
swasaaatrsmramay	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::swasaaatrsmramay
wkggcgssy	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::wkggcgssy
yyccmdgccmcdcccyycc	pT	3	2.36456558773424e-05	NA	IVA	cisBP19	p10_IVA	p10_IVA:::yyccmdgccmcdcccyycc
AHR	pT	3	0.000544313725489733	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::AHR
CEBPG	pT	3	9.19205298013245e-05	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CEBPG
CREBL2	pT	3	9.19205298013245e-05	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CREBL2
E2F1	pT	5	0.0359301672240801	NA	IVN	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F1
E2F2	pT	5	0.00176815286624184	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F2
E2F4	pT	5	0.00841212121212122	NA	IVN	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F4
E2F7	pT	3	0.0133118128654969	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F7
EGR2	pT	4	0.0178101792114695	TRUE	IVN	cisBP19	p10_IVN_Glu_IVA	p10_IVN_Glu_IVA:::EGR2
FBXL19,KDM2A	pT	3	0.0091425149700598	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::FBXL19,KDM2A
HHEX	pT	3	9.19205298013245e-05	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::HHEX
KLF15	pT	5	0.0205225714285716	NA	IVN	cisBP19	p0_IVA_p10_IVA_p10_IVN	p0_IVA_p10_IVA_p10_IVN:::KLF15
KLF4	pT	3	0.0389707692307692	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::KLF4
NHLH1	pT	4	0.00413813664596304	NA	IVN	cisBP19	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN:::NHLH1
NR3C1	pT	3	0.00364349999999979	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::NR3C1
SP4	pT	3	0.0178101792114695	TRUE	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::SP4
TFAP4	pT	4	0.000716387096773916	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::TFAP4
TFDP1,TFDP2,TFDP3	pT	4	0.000182631578947551	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::TFDP1,TFDP2,TFDP3
WT1	pT	4	0.0479650632911394	NA	IVN	cisBP19	p0_IVN_p10_IVA_p10_IVN	p0_IVN_p10_IVA_p10_IVN:::WT1
ZIC3	pT	3	0.0409790476190475	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZIC3
ZNF263	pT	5	0.0105752380952377	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF263
ZNF460	pT	3	0.0178101792114695	TRUE	IVN	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::ZNF460
ZNF571	pT	4	0.0178101792114695	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF571
ZNF8	pT	3	9.19205298013245e-05	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF8
cccrgccccrccchycycc	pT	3	0.0276612099644131	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cccrgccccrccchycycc
cccrgccmcrccch	pT	3	0.00245974683544257	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cccrgccmcrccch
cgtgygcam	pT	3	0.000716387096773916	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::cgtgygcam
grgggcgggrmr	pT	3	0.0178101792114695	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::grgggcgggrmr
rgggggcgggrmmw	pT	3	0.0151563218390806	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::rgggggcgggrmmw
rgsmycagctgcgyy	pT	3	9.19205298013245e-05	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::rgsmycagctgcgyy
shgrcgcsrvcrsms	pT	3	0.00869590361445817	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::shgrcgcsrvcrsms
swasaaatrsmramay	pT	3	0.00176815286624184	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::swasaaatrsmramay
wkggcgssy	pT	3	0.00331723270440293	NA	IVN	cisBP19	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::wkggcgssy
ZBTB14	pT	3	2.05970149253731e-05	NA	IVA	Jasp20	p10_IVA_p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVA_p10_IVN_Glu_IVA:::ZBTB14
ZNF135	pT	3	2.05970149253731e-05	NA	IVA	Jasp20	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF135
ZBTB14	pT	3	0.00503999999999971	NA	IVN	Jasp20	p10_IVA_p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVA_p10_IVN_Glu_IVA:::ZBTB14
ZNF135	pT	3	0.00368000000000028	NA	IVN	Jasp20	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::ZNF135
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pT	6	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CTCTGCCaC	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CTCTGCCaC
E2F1_17	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F1_17
EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic
FOS_GM12878_encode-Snyder_seq_hsa_r1:AlignACE_1_Intergenic	pT	5	2.13276231263383e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::FOS_GM12878_encode-Snyder_seq_hsa_r1:AlignACE_1_Intergenic
GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pT	5	2.13276231263383e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA:::GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA:::HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic
MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic
SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic	pT	4	2.13276231263383e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	pT	4	0.0363844897959183	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pT	6	0.000349473684210229	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CTCTGCCaC	pT	4	5.85882352941176e-05	NA	IVN	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::CTCTGCCaC
E2F1_17	pT	4	5.85882352941176e-05	NA	IVN	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::E2F1_17
EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic	pT	4	0.00318720000000003	NA	IVN	RSAT17	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic
FOS_GM12878_encode-Snyder_seq_hsa_r1:AlignACE_1_Intergenic	pT	5	0.00206068965517257	NA	IVN	RSAT17	p10_IVA_p10_IVN	p10_IVA_p10_IVN:::FOS_GM12878_encode-Snyder_seq_hsa_r1:AlignACE_1_Intergenic
GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	pT	5	0.00765347368421062	NA	IVN	RSAT17	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA:::GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic	pT	4	0.0274495693779904	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic
SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic	pT	4	0.00865633507853401	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic
EGR4	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	p0_IVN_Hip_IVA_Glu_IVA	p0_IVN_Hip_IVA_Glu_IVA:::EGR4
ETV3L	Hip	5	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA_Glu_IVA	Hip_IVA_Glu_IVA:::ETV3L
FOXA1	Hip	5	2.59618717504333e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::FOXA1
FOXD4,FOXD4L1,FOXD4L3,FOXD4L5,FOXD4L6,FOXI3,FOXP4,FOXS1	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA	Hip_IVA:::FOXD4,FOXD4L1,FOXD4L3,FOXD4L5,FOXD4L6,FOXI3,FOXP4,FOXS1
HNF4A	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::HNF4A
HOXB7,HOXC5,HOXC6	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::HOXB7,HOXC5,HOXC6
HOXC5,HOXC6	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA	Hip_IVA:::HOXC5,HOXC6
NHLH1	Hip	4	2.59618717504333e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN:::NHLH1
PAX4	Hip	5	2.59618717504333e-05	NA	IVA	cisBP19	p10_IVA_Hip_IVA	p10_IVA_Hip_IVA:::PAX4
PAX5	Hip	6	2.59618717504333e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::PAX5
SOX1	Hip	5	2.59618717504333e-05	NA	IVA	cisBP19	Hip_IVA	Hip_IVA:::SOX1
ZBTB12	Hip	5	2.59618717504333e-05	NA	IVA	cisBP19	p10_IVA_Hip_IVA_Hip_IVN	p10_IVA_Hip_IVA_Hip_IVN:::ZBTB12
FOXA1	Hip	5	8.65895953757226e-05	NA	IVN	cisBP19	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::FOXA1
HNF4A	Hip	4	8.65895953757226e-05	NA	IVN	cisBP19	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::HNF4A
HOXB7,HOXC5,HOXC6	Hip	4	8.65895953757226e-05	NA	IVN	cisBP19	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::HOXB7,HOXC5,HOXC6
NHLH1	Hip	4	0.0150627624309396	NA	IVN	cisBP19	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN	p10_IVA_p10_IVN_Hip_IVA_Hip_IVN:::NHLH1
ZBTB12	Hip	5	0.00612818181818228	NA	IVN	cisBP19	p10_IVA_Hip_IVA_Hip_IVN	p10_IVA_Hip_IVA_Hip_IVN:::ZBTB12
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CGkCTaCGm	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::CGkCTaCGm
FOS_HUVEC_encode-Snyder_seq_hsa_r1:AlignACE_2_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::FOS_HUVEC_encode-Snyder_seq_hsa_r1:AlignACE_2_Intergenic
GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA:::GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
GATA3	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::GATA3
GTGAGCCACC	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::GTGAGCCACC
HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic	Hip	5	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA:::HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic
MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::MYC_HUVEC_encode-Crawford_seq_hsa_r1:AlignACE_1_Intergenic
RAD21_HepG2_encode-Snyder_seq_hsa_IgG-rab_r1:AlignACE_3_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_Hip_IVA	p0_IVA_Hip_IVA:::RAD21_HepG2_encode-Snyder_seq_hsa_IgG-rab_r1:AlignACE_3_Intergenic
SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA:::SRF_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_3_Intergenic
T1ISRE	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::T1ISRE
mcsyGacyAACsghmyycaCrCcy	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::mcsyGacyAACsghmyycaCrCcy
srSstGvsss	Hip	3	2.24489795918367e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_Hip_IVA	p0_IVA_p0_IVN_Hip_IVA:::srSstGvsss
CGkCTaCGm	Hip	3	0.00366666666666646	NA	IVN	RSAT17	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN	p0_IVA_p0_IVN_Hip_IVA_Hip_IVN:::CGkCTaCGm
FOS_HUVEC_encode-Snyder_seq_hsa_r1:AlignACE_2_Intergenic	Hip	3	0.0107116104868916	NA	IVN	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::FOS_HUVEC_encode-Snyder_seq_hsa_r1:AlignACE_2_Intergenic
GATA3	Hip	3	5.58375634517767e-05	NA	IVN	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::GATA3
GTGAGCCACC	Hip	3	0.0449530685920577	NA	IVN	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::GTGAGCCACC
T1ISRE	Hip	3	5.58375634517767e-05	NA	IVN	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::T1ISRE
mcsyGacyAACsghmyycaCrCcy	Hip	3	5.58375634517767e-05	NA	IVN	RSAT17	Hip_IVA_Hip_IVN	Hip_IVA_Hip_IVN:::mcsyGacyAACsghmyycaCrCcy
CTCF	Glu	6	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::CTCF
E2F1	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F1
E2F4	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::E2F4
EGR2	Glu	6	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVN_Glu_IVA	p10_IVN_Glu_IVA:::EGR2
EGR3	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::EGR3
EGR4	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p0_IVN_Hip_IVA_Glu_IVA	p0_IVN_Hip_IVA_Glu_IVA:::EGR4
ETV3L	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	Hip_IVA_Glu_IVA	Hip_IVA_Glu_IVA:::ETV3L
FOXI2,FOXI3,FOXJ3	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::FOXI2,FOXI3,FOXJ3
IRX6	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::IRX6
KLF12	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::KLF12
MECOM	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::MECOM
NR1H2,NR2C1,NR4A3	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::NR1H2,NR2C1,NR4A3
NR1H2,NR2F6,NR4A3	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::NR1H2,NR2F6,NR4A3
NR1H2,RARA	Glu	6	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::NR1H2,RARA
PAX6	Glu	8	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::PAX6
RUNX2	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::RUNX2
TCF4	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::TCF4
TFAP2C,TFAP2D	Glu	7	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::TFAP2C,TFAP2D
TFAP2D	Glu	6	2.62413793103448e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN:::TFAP2D
ZBTB14	Glu	5	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVA_p10_IVN_Glu_IVA:::ZBTB14
ZBTB26	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::ZBTB26
ZEB1	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA	Glu_IVA:::ZEB1
ZNF30	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_Glu_IVA	p10_IVA_Glu_IVA:::ZNF30
ZNF384	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p0_IVA_p0_IVN_Glu_IVA	p0_IVA_p0_IVN_Glu_IVA:::ZNF384
ZNF45	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF45
ZNF460	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	p10_IVA_p10_IVN_Glu_IVA	p10_IVA_p10_IVN_Glu_IVA:::ZNF460
ZNF543	Glu	6	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF543
ZNF740	Glu	4	2.62413793103448e-05	NA	IVA	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF740
MECOM	Glu	4	0.0202933333333337	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::MECOM
NR1H2,NR1H4	Glu	4	0.0170464000000001	NA	IVN	cisBP19	Glu_IVN	Glu_IVN:::NR1H2,NR1H4
NR1H2,NR2C1,NR4A3	Glu	4	0.000419862068965937	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::NR1H2,NR2C1,NR4A3
NR1H2,RARA	Glu	6	0.00208493150684909	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::NR1H2,RARA
TCF4	Glu	4	0.0452646753246756	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::TCF4
TFAP2D	Glu	6	0.000106433566433566	NA	IVN	cisBP19	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_Glu_IVA_Glu_IVN:::TFAP2D
ZNF45	Glu	4	0.000106433566433566	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF45
ZNF543	Glu	6	0.0002113888888891	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF543
ZNF740	Glu	4	0.00911129251700672	NA	IVN	cisBP19	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::ZNF740
SNAI1	Glu	3	2.33846153846154e-05	NA	IVA	Jasp20	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::SNAI1
SNAI3	Glu	3	2.33846153846154e-05	NA	IVA	Jasp20	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::SNAI3
SNAI1	Glu	3	5.96078431372549e-05	NA	IVN	Jasp20	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::SNAI1
SNAI3	Glu	3	0.000350769230768933	NA	IVN	Jasp20	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::SNAI3
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	Glu	7	2.28519855595668e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	Glu	7	2.28519855595668e-05	NA	IVA	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CGaGCGSCw	Glu	4	2.28519855595668e-05	NA	IVA	RSAT17	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::CGaGCGSCw
EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic	Glu	5	2.28519855595668e-05	NA	IVA	RSAT17	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic
GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	Glu	4	2.28519855595668e-05	NA	IVA	RSAT17	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA	p10_IVA_p10_IVN_Hip_IVA_Glu_IVA:::GABPA_K562_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic	Glu	5	2.28519855595668e-05	NA	IVA	RSAT17	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA	p0_IVA_p10_IVA_Hip_IVA_Glu_IVA:::HEY1_K562_encode-Myers_seq_hsa_r1:AlignACE_1_Intergenic
BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic	Glu	7	0.00449100917431212	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::BCLAF1_K562_encode-Myers_seq_hsa_M33-P5B11_r1:AlignACE_3_Intergenic
BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic	Glu	7	0.0030040677966102	NA	IVN	RSAT17	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN	p0_IVA_p0_IVN_p10_IVA_p10_IVN_Hip_IVA_Glu_IVA_Glu_IVN:::BHLHE40_HepG2_encode-Myers_seq_hsa_v041610.1_r1:AlignACE_2_Intergenic
CGaGCGSCw	Glu	4	0.0028254077253217	NA	IVN	RSAT17	Glu_IVA_Glu_IVN	Glu_IVA_Glu_IVN:::CGaGCGSCw
EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic	Glu	5	0.00357276073619632	NA	IVN	RSAT17	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN	p0_IVN_p10_IVA_p10_IVN_Glu_IVA_Glu_IVN:::EP300_H1-hESC_encode-Myers_seq_hsa_v041610.2_r1:AlignACE_3_Intergenic
ELF1	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXA1	pZ	6	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
HDAC2	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
HF1H3B	pZ	5	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
HNF4G	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
MAFK	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
NANOG	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
REST	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SP1	pZ	6	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
STAT3	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TBP	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
UA9	pZ	4	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ZNF281	pZ	5	2.61475409836066e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
HF1H3B	pZ	4	2.61475409836066e-05	NA	IVA	Strong_motifbreakr	NA	NA
HNF4G	pZ	4	2.61475409836066e-05	NA	IVA	Strong_motifbreakr	NA	NA
SP1	pZ	5	2.61475409836066e-05	NA	IVA	Strong_motifbreakr	NA	NA
STAT3	pZ	4	2.61475409836066e-05	NA	IVA	Strong_motifbreakr	NA	NA
ELF1	pZ	4	0.000817948717948718	NA	IVN	AnyStr_motifbreakr	NA	NA
FOXA1	pZ	6	9.11428571428571e-05	NA	IVN	AnyStr_motifbreakr	NA	NA
HDAC2	pZ	4	0.0228493023255814	TRUE	IVN	AnyStr_motifbreakr	NA	NA
HF1H3B	pZ	5	9.11428571428571e-05	NA	IVN	AnyStr_motifbreakr	NA	NA
HNF4G	pZ	4	0.0228493023255814	TRUE	IVN	AnyStr_motifbreakr	NA	NA
MAFK	pZ	4	9.11428571428571e-05	NA	IVN	AnyStr_motifbreakr	NA	NA
NANOG	pZ	4	0.00355502487562192	NA	IVN	AnyStr_motifbreakr	NA	NA
REST	pZ	4	0.00675529411764706	TRUE	IVN	AnyStr_motifbreakr	NA	NA
TBP	pZ	4	0.0164645161290324	NA	IVN	AnyStr_motifbreakr	NA	NA
UA9	pZ	4	9.11428571428571e-05	NA	IVN	AnyStr_motifbreakr	NA	NA
ZNF281	pZ	5	0.00229680000000017	NA	IVN	AnyStr_motifbreakr	NA	NA
HF1H3B	pZ	4	9.11428571428571e-05	NA	IVN	Strong_motifbreakr	NA	NA
HNF4G	pZ	4	9.11428571428571e-05	NA	IVN	Strong_motifbreakr	NA	NA
KLF4	pT	4	2.73267326732673e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
NHLH1	pT	4	2.73267326732673e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SP3	pT	4	2.73267326732673e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TOPORS	pT	5	2.73267326732673e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SP3	pT	4	2.73267326732673e-05	NA	IVA	Strong_motifbreakr	NA	NA
TOPORS	pT	4	2.73267326732673e-05	NA	IVA	Strong_motifbreakr	NA	NA
KLF4	pT	4	0.00347555555555563	NA	IVN	AnyStr_motifbreakr	NA	NA
NHLH1	pT	4	0.00616648648648603	NA	IVN	AnyStr_motifbreakr	NA	NA
SP3	pT	4	0.0113279999999996	NA	IVN	AnyStr_motifbreakr	NA	NA
TOPORS	pT	5	0.000125454545454545	NA	IVN	AnyStr_motifbreakr	NA	NA
SP3	pT	4	0.000125454545454545	NA	IVN	Strong_motifbreakr	NA	NA
TOPORS	pT	4	0.00226990654205605	NA	IVN	Strong_motifbreakr	NA	NA
EGR1	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
EP300	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ETS1	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXC1	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXD3	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXJ2	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXJ3	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
HDAC2	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
NR2F6	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
PAX5	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
RAD21	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
RARA	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
RUNX1	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SIX5	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SOX14	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SOX2	Glu	6	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SOX21	Glu	7	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SOX3	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SP1	Glu	6	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SP2	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SPI1	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SRY	Glu	9	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TCF3	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TFAP2A	Glu	6	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TFAP2B	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TFAP2C	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TP53	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ZEB1	Glu	4	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ZNF143	Glu	5	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ZNF219	Glu	6	2.67432950191571e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
EGR1	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
NR2F6	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
SOX2	Glu	6	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
SOX21	Glu	6	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
SOX3	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
SPI1	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
SRY	Glu	8	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
TCF3	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
TFAP2A	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
TFAP2C	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
ZEB1	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
ZNF143	Glu	4	2.67432950191571e-05	NA	IVA	Strong_motifbreakr	NA	NA
NR2F6	Glu	4	0.0499473366834172	NA	IVN	AnyStr_motifbreakr	NA	NA
SOX14	Glu	4	0.000517037037037554	NA	IVN	AnyStr_motifbreakr	NA	NA
SOX21	Glu	7	0.000132952380952381	NA	IVN	AnyStr_motifbreakr	NA	NA
SOX3	Glu	4	0.0173556756756754	NA	IVN	AnyStr_motifbreakr	NA	NA
SP2	Glu	4	0.0260196858638744	NA	IVN	AnyStr_motifbreakr	NA	NA
SRY	Glu	9	0.000132952380952381	NA	IVN	AnyStr_motifbreakr	NA	NA
TFAP2A	Glu	6	0.00494159292035344	NA	IVN	AnyStr_motifbreakr	NA	NA
TFAP2B	Glu	4	0.000132952380952381	NA	IVN	AnyStr_motifbreakr	NA	NA
TFAP2C	Glu	5	0.000517037037037554	NA	IVN	AnyStr_motifbreakr	NA	NA
TP53	Glu	4	0.0290713402061859	NA	IVN	AnyStr_motifbreakr	NA	NA
ZEB1	Glu	4	0.0494945454545456	NA	IVN	AnyStr_motifbreakr	NA	NA
ZNF219	Glu	6	0.0159326086956521	NA	IVN	AnyStr_motifbreakr	NA	NA
CTCF	Glu	5	0.0138837158469949	NA	IVN	Strong_motifbreakr	NA	NA
NR2F6	Glu	4	0.02661125	NA	IVN	Strong_motifbreakr	NA	NA
SOX2	Glu	6	0.0105475555555556	NA	IVN	Strong_motifbreakr	NA	NA
SOX21	Glu	6	0.000132952380952381	NA	IVN	Strong_motifbreakr	NA	NA
SOX3	Glu	4	0.000132952380952381	NA	IVN	Strong_motifbreakr	NA	NA
SRY	Glu	8	0.000132952380952381	NA	IVN	Strong_motifbreakr	NA	NA
TFAP2A	Glu	4	0.0289326424870467	NA	IVN	Strong_motifbreakr	NA	NA
TFAP2C	Glu	4	0.000517037037037554	NA	IVN	Strong_motifbreakr	NA	NA
EP300	Hip	5	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXG1	Hip	5	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXJ2	Hip	5	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXJ3	Hip	6	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXP1	Hip	5	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
NANOG	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
NR2C2	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
PAX4	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
PPARG	Hip	6	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
REST	Hip	6	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
SIN3A	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
TCF3	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
ZNF263	Hip	4	2.64150943396226e-05	NA	IVA	AnyStr_motifbreakr	NA	NA
FOXG1	Hip	4	2.64150943396226e-05	NA	IVA	Strong_motifbreakr	NA	NA
FOXJ3	Hip	5	2.64150943396226e-05	NA	IVA	Strong_motifbreakr	NA	NA
PPARG	Hip	4	2.64150943396226e-05	NA	IVA	Strong_motifbreakr	NA	NA
TCF3	Hip	4	2.64150943396226e-05	NA	IVA	Strong_motifbreakr	NA	NA
FOXG1	Hip	5	0.00457425742574292	NA	IVN	AnyStr_motifbreakr	NA	NA
FOXJ3	Hip	6	0.0488888888888889	NA	IVN	AnyStr_motifbreakr	NA	NA
FOXP1	Hip	5	0.00410666666666667	NA	IVN	AnyStr_motifbreakr	NA	NA
NANOG	Hip	4	0.0374141025641026	NA	IVN	AnyStr_motifbreakr	NA	NA
NR2C2	Hip	4	0.00867184466019455	NA	IVN	AnyStr_motifbreakr	NA	NA
PAX4	Hip	4	0.0263247863247863	NA	IVN	AnyStr_motifbreakr	NA	NA
PPARG	Hip	6	0.00244768211920548	NA	IVN	AnyStr_motifbreakr	NA	NA
SIN3A	Hip	4	0.0374141025641026	TRUE	IVN	AnyStr_motifbreakr	NA	NA
ZNF263	Hip	4	0.0104603773584905	NA	IVN	AnyStr_motifbreakr	NA	NA
FOXG1	Hip	4	0.00634117647058787	NA	IVN	Strong_motifbreakr	NA	NA
FOXJ3	Hip	5	0.0374141025641026	NA	IVN	Strong_motifbreakr	NA	NA
PPARG	Hip	4	0.0110306976744185	NA	IVN	Strong_motifbreakr	NA	NA
