#!/usr/bin/env bash

upstream=GTCGAC
downstream=GCGATCGC

for i in $(cat newsamplenames.txt)
do
	cat $i | sed -n '/^[ATCGN]\{150\}/p' > "$i"_clean.txt
	grep -v "GGGGGGGGGGGGGG" "$i"_clean.txt > "$i"_final.txt
	sed -n 's/^.*'"$upstream"'\([[:upper:]]\{10\}\)'"$downstream"'.*$/\1/p' "$i"_final.txt > "$i"_foundBCs.txt
	rm "$i"_clean.txt
	rm "$i"_final.txt
done