## Bash-based barcode alignment:
The R script is annotated line-by-line to explain what it’s doing. Basically, you run the “Align Only . sh” file from this directory on your un-gzip’d .fastq file, which takes about ~2min per 1 mil reads. The last output from that, which will be the .fastq’s name followed by ‘foundBCs.txt’ then goes into R for the counting, which really only takes about as much time as it takes the end user to fill in the appropriate files. Example files are in the directory with the R script so you can see what the inputs would look like.

## mpra4mpra: Script for processing MPRA barcode counts and assessing significance with custom filters and three options for allele-differential calling. (IN THIS PAPER, ONLY used for filtering and calculating single-barcode expression levels).
### Current version: 1.7.1, 06/21/21.
#### Input objects:
1. Counts directory (barcode count files only!), OR counts in an .RDS format with a column named identically to the library metadata table--said column is used to merge in the counts with the metadata for processing, and is specified by "meta.BC.col"
2. Filepath for the DNA barcode counts (ONLY if not be in the counts directory)
3. Metadata sheet (containing at bare minimum the barcode sequences as read from the sequencing data, identifiers by which to group a set of barcodes to an identical regulatory sequence, and identifiers by which to group those barcode sets to a superset (e.g., an rsID, an allele, and a barcode column))
##### Regarding inputs, user must define:
1. Column containing barcode sequences (by name or by column index)
2. Column identifying 'elements' (to group barcodes to an individual sequence or ONE variant thereof)
3. Column identifying 'elem.group' (to group alleles into SNPs, for e.g.)
4. **Assumes DNA counts are coming from a single sample** (i.e. from input plasmid or virus) 

#### Input parameters (analytic):
1. RNA barcode seq count threshold for inclusion
2. DNA barcode seq count threshold for inclusion
3. Minimum number of barcodes above threshold per element for inclusion
4. Use counts or cpm?
5. Normalize expression to that of a 'basal' set of barcodes within each sample (i.e. within-sample normalization to an internal control?) (if so, declare basals' identifier used in 'elements'). Note that for the mean and median approaches, the (mean or median) expression without normalization is calculated in each sample for each element, and then the (respective mean or median) basal element's expression in that sample is subtracted from the (respective central measure) for each other element in the sample.
6. 'paired.t' : Specify whether a paired or unpaired t.test is desired when using the sum or mean ttest methods below. (default=FALSE). Recommended for more biologically complex model systems.
	a) If this returns an error containing a part that "data are essentially constant", then a paired t.test is neither necessary nor appropriate for the application in question and you should change this back to FALSE.
7. 'compare.method' (NEW OPTIONS v1.7) : Determine element expression for simple two-condition (allele) testing as: 
	a) 'sum.ttest': t test of replicatewise aggregated barcodes sum (log2(sum(element RNA barcodes) / sum (element DNA barcodes))
	b) 'mean.ttest': t-test of replicatewise aggregated barcodes mean (mean ( log2(element RNA barcode/element DNA barcode) ) ), or
	c) 'median.ttest': t-test of replicatewise median barcode expression per element (i.e. allele)
	d) 'lmm' : using repeated-measures lmm with barcode-level expression values as input
8. Bootstrapping -- simulate *bootstrap.iter* "allelic" tests between randomly selected sets of *bootstrap.nBCs* subsetted from among a specified single element *bootstrap.elem.id* (e.g. basal promoter set), and generate a vector of test statistics (t-values or F values) for the corresponding analysis method to correct p-values (using the *qvalue package*). This appears to be robust to the number of barcodes used for each simulation when the number of simulated tests is large; however, for accuracy's sake a good value to use would be the median number of barcodes analyzed among actual single-allele elements in the library.
9. NEW version 1.7: If using basal normalization in expression calculations of the data, users have the option to set bnorm.bootstrap=TRUE to perform the equivalent basal normalization procedure on the expression values being calculated and t-tested in each bootstrap iteration. Note that, when using basal barcodes to bootstrap, this option should only be used if the number of barcodes being drawn in each bootstrap iteration is <<< the number of total basal barcodes. (Otherwise, the basal-normalized value for the bootstrapped subset of barcodes will be very close to the central measure for ALL basal barcodes, resulting in ~0 for each sample's bootstrapped expression value and thus grossly low-error test statistics). Otherwise, with 10x or more total basal barcodes compared to the number used per bootstrap iteration, this actually improves stringency of significance calling in basal-normalized datasets. 

#### Filtering steps, in order:
1. Filter out barcodes < minimum DNA count threshold (*'DNA.thresh'*)
2. Filter out barcode SETS from all replicates if the above step leaves fewer than the specified minimum number of barcodes per element for analysis (*'nBC.thresh'*) 
3. Filter out singular barcodes < minimum RNA count threshold (separately for each replicate) (*'RNA.thresh'*)
4. Filter out outlier barcode expression values (defined by number of standard deviations from the barcode's group (i.e., CRE or allele or etc) mean (*'outlierBC.nSDs'*) from each replicate separately. **Yes, means are recalculated with outliers removed**
5. From ALL samples, drop barcodes expressed in fewer than the minimum number of replicates specified for barcode inclusion in analysis (*'nRep.BCthresh'*).
6. From INDIVIDUAL samples, drop barcode sets that are below the minimum number of barcodes required in a given replicate for analysis of that element in that replicate (*'nBC.thresh'*, same parameter as number 2 above).

#### Output parameters:.
1. FDR threshold for elements to be returned. Default: 0.05, using Benjamini-Hochberg correction.
2. Report null elements? (output object of elements for which RNA barcodes are 0 in >1 replicate though DNA counts passed threshold.) Default: TRUE
3. nulls.n : if ^ = TRUE, how many replicates need to have shown the null effect (i.e. RNA/DNA = 0 with DNA > 0) to be reported back? (default=1)
4. Samplewise expression for nulls T/F. (T = return a null elements object with columns of expression for each sample so long as at least *nulls.n* samples showed null effect, plus mean expression of that element across the replicates. If report = TRUE but samplewise.nulls = FALSE, just returns elements with 1+ null and the mean expression). Default: FALSE
5. Stepwise dropout? Reports back number of barcodes, alleles, and allele pairs lost at each filtering step. 
