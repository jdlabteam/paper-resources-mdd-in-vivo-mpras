rsid_only: MPRA library SNP rsid.
sources: Internal code for the source GWAS.
ldblock_id: Similar to above, but also identifying the particular LD region from the GWAS.
snp_hg19_coord: Chromosome and coordinate of the SNP in hg19 coordinates as retrieved from dbSNP 151.
chr_hg38: Chromosome of the SNP from lifting over hg19 coordinates.
start_hg38: Coordinate resulting from lifting over SNP hg19 coordinate to hg38.
end_hg38: Coordinate resulting from lifting over SNP hg19 coordinate to hg38. 
Alleles: dbSNP151 alleles field for the SNP.
MAF: Minor allele frequency in the 1000 Genomes Release 3 query population for the SNP (EUR for all loci except the two CONVERGE loci). 
R2: Linkage between library SNP and its GWAS tag in R2 for the query population. 
tag_snp: GWAS tag SNP used for LD expansion to acquire library SNP.
ref.allele: dbSNP151-derived reference allele.
alt.allele: dbSNP151-derived alternative allele.
PEC_allEnh_ID: PsychENCODE "all enhancers" dataset identifier, if such an enhancer overlaps the SNP.
PEC_hiconf_enhID: Ibid for the "high confidence" enhancer subset.
fantom_astro: Overlaps a FANTOM5 enhancer RNA (eRNA) from astrocytes.
fantom_neuron: Overlaps FANTOM5 erna from neurons.
fantom_nsc: Overlaps FANTOM5 erna from neural stem cells.
fantom_brain: Overlaps FANTOM5 erna from human brain tissue.
CMC_gene: Commonmind dlPFC eQTL gene(s), if any.
CMC_FDR: Commonmind eQTL FDR(s), if any.
CMC_up_allele: Commonmind upregulating allele for each eQTL gene. 
CMC_down_allele: Commonmind downregulating allele for each eQTL gene. 
GTEX_gene: GTEX brain eqtl gene (any of 12 brain regions)
GTEX_p_nom: GTEX brain eqtl nominal p-value
GTEX_tissue: GTEX regions from which an eQTL was identified.
PEC_eqtl_gene: PsychENCODE dlpfc eqtl gene(s), if any.
PEC_eqtl_FDR: PsychENCODE dlpfc eqtl FDR(s), if any.
PFC_27ac: Psychencode prefrontal cortex H3K27ac (enhancer-like) chromatin mark overlaps
Temporal_27ac: Psychencode temporal cortex H3K27ac (enhancer-like) chromatin mark overlaps
cerebell_27ac: Psychencode cerebellar cortex H3K27ac (enhancer-like) chromatin mark overlaps
dentate1_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate1_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate1_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate1_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate2_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate2_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate2_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
dentate2_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, dentate-like culture.
astro1_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro1_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro1_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro1_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro2_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro2_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro2_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
astro2_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
cortex1_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, primary astrocyte culture.
cortex1_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex1_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex1_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex2_Targeted_Promoters: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex2_Targeted_Misc_Promoters: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex2_ATAC_Promoter_Peaks: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
cortex2_ATAC_Other_Peaks: Column from Song 2019 human neural cell type Hi-C, cortical neuron  culture.
ROSMAP_eqtl_gene: ROSMAP human brain eQTL gene(s) if any
ROSMAP_eqtl_rho: ROSMAP human brain eQTL effect size(s), if any
ROSMAP_eqtl_pval: ROSMAP human brain eQTL pvalue(s)
rosmap_eqtl_celltypes: ROSMAP deconvoluted cell type-driven eQTLs
rosmap_eqtl_celltype_genes: ROSMAP deconvoluted cell type-driven eQTL genes
rosmap_eqtl_celltype_intrxn_pval: ROSMAP deconvoluted cell type-driven eQTL pvalues
rosmap_haqtl: ROSMAP human brain histone acetylation QTL, if any 
rosmap_haqtl_rho: ROSMAP human brain histone acetylation QTL effect size, if any
rosmap_haqtl_pval:  ROSMAP human brain histone acetylation QTL p-value
rosmap_mqtl_cpgid:  ROSMAP human brain methylation QTL cpg island identifier
rosmap_mqtl_rho:  ROSMAP human brain methylation QTL effect size
rosmap_mqtl_pval:  ROSMAP human brain methylation QTL p-value
FANTOM_Robust_enh: FANTOM5 "robust enhancer" identifier, if one overlaps SNP
FANTOM_Robust_HGNCid: FANTOM5 "robust enhancer" target gene (as inferred by FANTOM using eRNA-gene expression correlations)
PEC_TAR: PsychENCODE "Transcriptionally Active Region" overlap.
amiri_iPS_regulator: iPSC regulators in Amiri 18, which are classified as activating or repressive.
fetal_ctx_regs: Fetal cortex regulators in Amiri 18, which are classified as activating or repressive
ctx_onoid_td0_regs: Cortical organoid, culture day 0 regulators in Amiri 18, which are classified as activating or repressive.
ctx_onoid_td11_regs: Cortical organoid, culture day 11 regulators in Amiri 18, which are classified as activating or repressive.
ctx_onoid_td30_regs: Cortical organoid, culture day 30 regulators in Amiri 18, which are classified as activating or repressive.
ENScr_female_adult_neocortex_enhancerlike: ENCODE SCREEN cis-regulator segmentations from female adult neocortex (enhancerlike state).
ENScr_female_adult_neocortex_promoterlike: ENCODE SCREEN cis-regulator segmentations from female adult neocortex (promoterlike state).
ENScr_female_fetal_openchrom: ENCODE SCREEN cis-regulator segmentations from female fetal brain (open chromatin).
ENScr_female_fetal_promoterlike: ENCODE SCREEN cis-regulator segmentations from female adult neocortex (promoterlike state).
ENScr_male_adult_basalganglia_enhancerlike: ENCODE SCREEN cis-regulator segmentations from male adult basal ganglia (enhancerlike state).
ENScr_male_adult_basalganglia_openchrom:  ENCODE SCREEN cis-regulator segmentations from male adult basal ganglia (open chromatin).
ENScr_male_adult_basalganglia_promoterlike:  ENCODE SCREEN cis-regulator segmentations from male adult basal ganglia (promoterlike state).
ENScr_male_adult_cerebellarctx_openchrom:  ENCODE SCREEN cis-regulator segmentations from male adult cerebellum (open chromatin).
ENScr_male_adult_neocortex_enhancerlike: ENCODE SCREEN cis-regulator segmentations from male adult cortex (enhancerlike state).
ENScr_male_adult_neocortex_openchrom: ENCODE SCREEN cis-regulator segmentations from male adult cortex (open chromatin).
ENScr_male_adult_neocortex_promoterlike: ENCODE SCREEN cis-regulator segmentations from male adult cortex (promoterlike state).
ENScr_male_adult_subcortical_enhancerlike: ENCODE SCREEN cis-regulator segmentations from male adult subcortex (enhancerlike state).
ENScr_male_adult_subcortical_openchrom: ENCODE SCREEN cis-regulator segmentations from male adult cortex (open chromatin).
ENScr_male_adult_subcortical_promoterlike: ENCODE SCREEN cis-regulator segmentations from male adult cortex (promoterlike state).
ENScr_male_adult_subnigra_enhancerlike: ENCODE SCREEN cis-regulator segmentations from male adult substantia nigra (enhancerlike state).
ENScr_male_adult_subnigra_promoterlike: ENCODE SCREEN cis-regulator segmentations from male adult substantia nigra (promoterlike state).
ENScr_male_fetal_openchrom: ENCODE SCREEN cis-regulator segmentations from male fetal brain (open chromatin).
ENScr_nosex_fetal_openchrom: ENCODE SCREEN cis-regulator segmentations from fetal brain (no sex specified; open chromatin).
