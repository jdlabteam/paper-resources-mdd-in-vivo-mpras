IMPORTANT: only the single-barcode expression values (columns ending in "BCxpr") and mean expression columns (starting with "mean" and ending "BCxpr"; only used to pull the mean basal expression level in each sample for normalizing remaining barcode expression levels). 

In other words, the statistical tests in these 8 tables were merely run as part of the existing function for analysis of this library, which carries out the complex pre-analysis filtering steps and barcode expression calculations and was used solely for that purpose in these analyses. The statistical test results in these tables are NOT the actual tests used -- the extracted barcode expression values from these tables were then basal-normalized within-sample and run through the linear mixed model with random barcode effects as described in the methods to generate the reported results.

Each of these RDS objects contains five data tables:
[[1]]: results table with barcode expression values
[[2]]-[[5]] various filtering/QC reports from handling the data (number of barcodes dropped at each filter step, number of adequately counted DNA barcodes absent or effectively absent from RNA, etc.)